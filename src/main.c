#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#define TIMER_PRESCALER 720
TIM_TimeBaseInitTypeDef timer;
volatile uint16_t LedOn = 1000;
uint16_t previousState;

int main(void)
{
  int i;
  int work = 1000;
  int delay = 1000;
  SystemInit();
  GPIO_InitTypeDef  GPIO_InitStructure;


  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOA, ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

  TIM_TimeBaseStructInit(&timer);
  timer.TIM_Prescaler = TIMER_PRESCALER - 1;
  timer.TIM_Period = 50;
  TIM_TimeBaseInit(TIM3, &timer);
  /* Configure the GPIO_LED pin */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);


  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
  GPIO_Init(GPIOC, &GPIO_InitStructure);


  /* Configure the GPIO_BUTTON pin */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);



  TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

  TIM_Cmd(TIM3, ENABLE);

  NVIC_EnableIRQ(TIM3_IRQn);
  uint8_t k1=0;
  uint8_t k2=0;
  uint8_t k3=0;
  uint8_t k4=0;

  while (1) {

	  if (!(k1||k2||k3||k4)){

	  if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_5)==1)
	  {
	  GPIO_ResetBits(GPIOC, GPIO_Pin_13);


	  TIM3->ARR = 65535;
	  TIM3->CNT=0;
	  k1=1;
	  }
	  }
	  if (!(k2||k3||k4)){
	  if ( GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)==1 ) {

	  work = 2000 - 1;
	  k2=1;
	  }

	  else if ((GPIO_ReadInputDataBit(GPIOC,
	  GPIO_Pin_15)==1)){

	  work = 3000 - 1;
	  k3=1;
	  }

	  else if ((GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_11))){

	  work = 4000 - 1;
	  k4=1;
	  }
	  else {
	  work = 1000;
	  }
	  }

	  if ((k1)&&(!(GPIO_ReadInputDataBit(GPIOB,
	  GPIO_Pin_5)==1))){
	  if (TIM3->CNT>10){
	  work = TIM3->CNT;
	  }
	  TIM3->ARR= work;
	  TIM3->CNT=0;
	  GPIOC-> ODR &= ~GPIO_ODR_ODR15;
	  k1=0;
	  }

	  if ((k2)&&(!(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)==1))){
	  k2=0;
	  }

	  else if ((k3)&&(!(GPIO_ReadInputDataBit(GPIOC,
	  GPIO_Pin_15)==1))){
	  k3=0;
	  }

	  else if ((k4)&&(!(GPIO_ReadInputDataBit(GPIOB,
	  GPIO_Pin_11)==1))){
	  k4=0;
	  }
  }
}

void TIM3_IRQHandler()
{
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	if (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13) == 1)
	{
	GPIO_ResetBits(GPIOC, GPIO_Pin_13);
	}
	else
	{
	GPIO_SetBits(GPIOE, GPIO_Pin_13);
	}
}

